"use strict";
///import Koa from 'koa';
///import views from 'koa-views';
///import session from 'koa-session';
///import  bodyParser  from 'koa-body';
Object.defineProperty(exports, "__esModule", { value: true });
// import {KoaRouter} from './server_router'
///import {KoaRouter} from './middleware/router';
///import { initDB }   from './db';
///import { methodOverride } from './middleware/method-override'
const express = require("express");
// import express = require('express');
// import session from "express-session";
const path = require("path");
const logger = require("morgan");
const cookieParser = require("cookie-parser");
const bodyParser = require("body-parser");
const stylus = require("stylus");
// import errorHandler from "errorhandler"
const config_1 = require("./config/config");
// import IndexRoute from "./routes/index.server.routes"
// import IndexController from './controllers/index.server.controller';
//TODO: make router a class, with  router instance as its implementation
// define which data members to expose in construcor
class ServerRun {
    // private router: KoaRouter;
    // define and initialze data members here...
    constructor(port) {
        this.port = port;
        this.app = express();
        this.router = express.Router();
    }
    start() {
        // initialize database
        ///    initDB();
        // this.app.keys = ['secret key'];
        // view engine setup
        this.app.set('views', path.join(__dirname, 'views'));
        this.app.set('view engine', 'jade');
        // register middlware
        /**
        * Error Handler. Provides full stack - remove for production
        */
        // this.app.use(errorHandler());
        // uncomment after placing your favicon in /public
        // this.app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
        this.app.use(logger('dev'));
        this.app.use(bodyParser.json());
        this.app.use(bodyParser.urlencoded({ extended: false }));
        this.app.use(cookieParser());
        this.app.use(stylus.middleware(path.join(__dirname, 'public')));
        this.app.use(express.static(path.join(__dirname, 'public')));
        // load app routes 
        for (let route of config_1.default.globFiles(config_1.default.routes)) {
            require(path.resolve(route)).default(this.app);
        }
        // this.app.use('/', In;
        //this.app.use('/users', users);
        const server = this.app.listen(this.port, () => {
            console.log(`Server process: ${process.pid} listen on port ${this.port}`);
        });
        // catch 404 and forward to error handler
        //     this.app.use(function(req :Request, res :Response, next :NextFunction ) {
        //     let err :Error  = new Error('Not Found');
        //     err.status = 404;
        //     next(err);
        //   });
        // error handler
        this.app.use(function (err, req, res, next) {
            // set locals, only providing error in development
            res.locals.message = err.message;
            res.locals.error = req.app.get('env') === 'development' ? err : {};
            // render the error page
            // res.status(err.code || 500);
            res.status(err.errno || 500);
            res.render('error');
        });
        // this.app.on("error", (e :Error) => console.log(`SEVERE ERROR: ${e.message}`) );
    }
}
exports.ServerRun = ServerRun;
//# sourceMappingURL=server_run.js.map