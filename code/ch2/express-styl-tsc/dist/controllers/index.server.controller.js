"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class IndexController {
    index(req, res, next) {
        res.render('index', { title: 'Express' });
    }
    msg(req, res) {
        res.json({ msg: 'Hello!' });
    }
}
exports.default = IndexController;
// export class instance...
exports.indexController = new IndexController();
//# sourceMappingURL=index.server.controller.js.map