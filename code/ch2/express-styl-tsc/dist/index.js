"use strict";
//@ts-check
Object.defineProperty(exports, "__esModule", { value: true });
// Run server for debug in single process without cluster
const server_run_1 = require("./server_run");
// start server
const port = process.env.PORT ? parseInt(process.env.PORT) : 3002;
const app = new server_run_1.ServerRun(port);
app.start();
//# sourceMappingURL=index.js.map