///import Koa from 'koa';
///import views from 'koa-views';
///import session from 'koa-session';
///import  bodyParser  from 'koa-body';

// import {KoaRouter} from './server_router'
///import {KoaRouter} from './middleware/router';
///import { initDB }   from './db';
///import { methodOverride } from './middleware/method-override'


import * as express   from 'express';
import { Express } from 'express';
// import express = require('express');
// import session from "express-session";
import * as  path from "path";
import  * as favicon from 'serve-favicon';
import * as logger from 'morgan';
import  * as cookieParser from 'cookie-parser';
import * as bodyParser from 'body-parser';
import  stylus = require('stylus');
import { Response, Request } from 'express-serve-static-core';
import { runInThisContext } from 'vm';
import { NextFunction } from 'connect';
import errorHandler = require("errorhandler");
import methodOverride = require("method-override");

//  //import config from './config/config';
// import IndexRoute from "./routes/index.server.routes"
// import IndexController from './controllers/index.server.controller';


 
//TODO: make router a class, with  router instance as its implementation

// define which data members to expose in construcor
export class ServerRun {
    private port: number;
    private app: express.Express;
    private router : any;
    // private router: KoaRouter;
   
 
    // define and initialze data members here...
    public constructor(port: number) {
        this.port = port;
        this.app = express();
        this.router = express.Router();
      
      
        
    }
 
    public start() {
        // initialize database
    ///    initDB();

        // this.app.keys = ['secret key'];
      
    
        this.config();

        //TODO: load app routes 
        
        // this.app.use('/', In;
    //this.app.use('/users', users);
        
        const server = this.app.listen(this.port, () => {
            console.log(`Server process: ${process.pid} listen on port ${this.port}`);
        });
 
    // catch 404 and forward to error handler
//     this.app.use(function(req :Request, res :Response, next :NextFunction ) {
//     let err :Error  = new Error('Not Found');
//     err.status = 404;
//     next(err);
//   });

    // error handler
    this.app.use(function(err :NodeJS.ErrnoException, req :Request, res :Response, next :NextFunction) {
        // set locals, only providing error in development
        res.locals.message = err.message;
        res.locals.error =  req.app.get('env') === 'development' ? err : {};
    
        // render the error page
        // res.status(err.code || 500);
        res.status(err.errno || 500)
        res.render('error');
     });
        // this.app.on("error", (e :Error) => console.log(`SEVERE ERROR: ${e.message}`) );
  }

  public config () {

           
        // view engine setup
        this.app.set('views', path.join(__dirname, 'views'));
        this.app.set('view engine', 'jade');

        // register middlware
        /**
        * Error Handler. Provides full stack - remove for production
        */
        // this.app.use(errorHandler());

        // uncomment after placing your favicon in /public
        // this.app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
        this.app.use(logger('dev'));
        this.app.use(bodyParser.json());
        this.app.use(bodyParser.urlencoded({ extended: false }));
        this.app.use(cookieParser());
        this.app.use(stylus.middleware(path.join(__dirname, 'public')));
        this.app.use(express.static(path.join(__dirname, 'public')));
   
          //use override middlware
        this.app.use(methodOverride());

        //catch 404 and forward to error handler
        this.app.use(function(err: any, req: express.Request, res: express.Response, next: express.NextFunction) {
            err.status = 404;
            next(err);
        });

        //error handling
        this.app.use(errorHandler());


  };

  public routes() {

  };

  public api() {

  };

   
}

