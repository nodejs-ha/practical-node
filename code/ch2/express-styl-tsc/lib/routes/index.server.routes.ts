// import * as express from 'express';
// let router = express.Router()

// /* GET home page. */
// router.get('/', function(req, res, next) {
//   res.render('index', { title: 'Express' })
// })

// module.exports = router

// import * as express  from 'express'
import { Express } from 'express'; 

import { indexController } from '../controllers/index.server.controller'; 


export default class IndexRoute { 
    constructor( app: Express) { 
        app.route('/')
                .get(indexController.index);
        app.route('/msg')
                .get(indexController.msg);

  }

}

